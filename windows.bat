: # This is a special script which intermixes both sh
: # and cmd code. It is written this way because it is
: # used in system() shell-outs directly in otherwise
: # portable code. See https://stackoverflow.com/questions/17510688
: # for details.
:; echo "This is ${SHELL}"; exit
@ECHO OFF
ECHO This is %COMSPEC%
set commitId=%1
set folderPath=%2
set pathCmd= 
echo %commitId%
echo %folderPath%
if [%folderPath%] == [] (set pathCmd= ) else (set pathCmd=-C %folderPath%)
echo %pathCmd%
git %pathCmd% fetch
git %pathCmd% reset --hard HEAD
git %pathCmd% checkout -b %commitId% %commitId%
git %pathCmd% push origin HEAD:test
git %pathCmd% checkout -B test origin/test
git %pathCmd% pull
git %pathCmd% branch -d %commitId%