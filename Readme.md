##Script to deploy to test server
Make sure the folder structure looks like this <br/>
```
|_frontend
|_backend
|_linux.sh
|_windows.bat
```
For windows: try

```
windows.bat <commitId> <frontend/backend>
or
./windows.bat <commitId> <frontend/backend>
```

For Linux/Mac OS: <br/>
`./linux.sh -c <commitId> -p <frontend/backend>`

```
<commitId>: Id of the commit that we want to deploy, have to be the commit after the last (HEAD) commit of the test branch
<frontend/backend>: folder of the project to deploy
```

to get the commitId, use:<br/>
```
git -C <frontend/backend> fetch
git -C <backend/frontend> log --pretty=oneline origin/<sprintNumber>
```
where `<sprintNumber>` is the branch of current sprint: sprint-14, sprint-15, sprint-16,...  