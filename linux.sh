#!/bin/bash

helpFunction(){
   echo ""
   echo "Usage: $0 -c commitId -p path"
   echo -e "\t-c commitId: c2a8ab87c2f162c510c5381359eb02278e9d8c2c,..."
   echo -e "\t-e path: frontend, backend"
   exit 1 # Exit script after printing help
}

while getopts "c:p:" opt
do
   case "$opt" in
      c ) commitId="$OPTARG" ;;
      p ) path="$OPTARG" ;;
      ? ) helpFunction ;; 
   esac
done

if [ -z "$commitId" ]
then
   echo "CommitId is required";
   helpFunction
fi
pathCmd=
if [ -z "$path" ]
then
      echo "\$path is empty"
else
      pathCmd="-C $path"
fi

# Begin script in case all parameters are correct
echo "$commitId"
echo "$pathCmd"
git $pathCmd fetch
git $pathCmd reset --hard HEAD
git $pathCmd checkout -b $commitId $commitId
git $pathCmd push origin HEAD:test
git $pathCmd checkout -B test origin/test
git $pathCmd pull
git $pathCmd branch -d $commitId
